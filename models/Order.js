
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	productId: {
		type: String,
		required: [true, "First name is required."]
	},

    purchasedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Order", orderSchema);
