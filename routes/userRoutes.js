const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../authentication/auth");

//Route for checking if email exists
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

//Route for User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;


//Routes for User Authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Today's Deliverables
//  - User Authentication
//  - JWT implementation
//  - Set User as Admin functionality

// Set User as Admin Sample Workflow
//  1. An authenticated admin user sends a PUT request containing a JWT in its header to the /:userId/setAsAdmin endpoint.
//  2. API validates JWT, returns false if validation fails.
//  3. If validation successful, API finds user with ID matching the userId URL parameter and sets its isAdmin property to true.

router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {

	let data = {
		userId: req.params.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

    if (data.isAdmin == true) {
        userController.makeAdmin(data).then(resultFromController => res.send(resultFromController));
    } else {
        res.send("Not authorized to make another user Admin.");
    }
})


router.post("/checkout", auth.verify, (req, res) => {

    let data = {
        userId: auth.decode(req.headers.authorization).id,
        productId: req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

	userController.createOrder(data).then(resultFromController => {
		res.send(resultFromController);
	})
})

router.get("/orders", auth.verify, (req, res) => {

    let data = auth.decode(req.headers.authorization);

    userController.retrieveAllOrders(data).then(resultFromController => {
        res.send(resultFromController);
    })

})

router.get("/myOrders", auth.verify, (req, res) => {

    let data = {
        userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    userController.retrieveOrder(data).then(resultFromController => {
        res.send(resultFromController);
    })
})
