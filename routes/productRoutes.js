// Create Product Sample Workflow
//   1. An authenticated admin user sends a POST request containing a JWT in its header to the /products endpoint.
//   2. API validates JWT, returns false if validation fails.
//   3. If validation successful, API creates product using the contents of the request body.

const express = require("express");
const router = express.Router();
// const userController = require("../controllers/userControllers");
const productController = require("../controllers/productControllers");
const auth = require("../authentication/auth");

// Route for creating products
router.post("/createProduct", (req, res) => {
    
	const data = { 
        reqBody: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };

    if (data.isAdmin == true) {
        productController.createProduct(data).then(resultFromController => res.send(resultFromController));
    } else {
        res.send("Not authorized as Admin to create product.");
    }
})


// Retrieve All Active Products Sample Workflow
//   1. A GET request is sent to the /products endpoint.
//   2. API retrieves all active products and returns them in its response. 

router.get("/active", (req, res) => {

    const reqBody = req.body;

    productController.getActiveProducts(reqBody).then(resultFromController => {
        res.send(resultFromController);
    })
})

// Retrieve Single Product Sample Workflow
//   1. A GET request is sent to the /products/:productId endpoint.
//   2. API retrieves product that matches productId URL parameter and returns it in its response.

router.get("/:productId", (req, res) => {

    console.log(req.params);

	productController.getSingleProduct(req.params).then(resultFromController => {
		res.send(resultFromController);
	})
})

router.put("/:productId", auth.verify, (req, res) => {

    const data = {
       productId: req.params.productId,
       isAdmin: auth.decode(req.headers.authorization).isAdmin,
       updatedProduct: req.body 
    }

    productController.updateProduct(data).then(resultFromController => {
        res.send(resultFromController);
    })
})

router.put('/:productId/archive', auth.verify, (req, res) => {
    const data = {
        productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.archiveProduct(data).then(resultFromController => {
        res.send(resultFromController);
    })
})

// router.put("/:productId/archive", auth.verify, (req, res) => {
    
//     const data = {
//         productId: req.params.productId,
//         isAdmin: auth.decode(req.headers.authorization).isAdmin
//     }

//     productController.archiveProduct(data).then(resultFromController => {
//         res.send(resultFromController);
//     })
// })

module.exports = router;

