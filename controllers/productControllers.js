
const bcrypt = require("bcrypt");
const req = require("express/lib/request");
const auth = require("../authentication/auth");
const Product = require("../models/Product");

// Creating Products
module.exports.createProduct = (data) => {
	
	let newProduct = new Product({
		name: data.reqBody.name,
		description: data.reqBody.description,
		price: data.reqBody.price,
		isActive: data.reqBody.isActive
	})
	
	
	return newProduct.save().then((product, error) => {
		
		if(error){
			return false;
		} else{
			return product;
		}
	})
}   

module.exports.getActiveProducts = (reqBody) => {
	return Product.find({isActive: true}).then(result => {
		if (result == null) {
			
			return false;
			
		} else {
			
			return result;
		}
	})
}

module.exports.getSingleProduct = (reqParams) => {
	
	return Product.findById(reqParams.productId).then(result => {
		
		if (result == null) {
			
			return "Product not found in the collections.";
			
		} else {
			
			return result;
		}
		
	})
}

module.exports.updateProduct = (data) => {
	console.log(data);
	
	return Product.findById(data.productId).then((result, error) => {
		
		console.log(result);
		
		if (data.isAdmin) {
			result.name = data.updatedProduct.name;
			result.description = data.updatedProduct.description;
			result.price = data.updatedProduct.price;
			
			console.log(result);
			
			return result.save().then((updatedProduct, error) => {
				
				if (error) {
					return false;
				} else {
					return updatedProduct;
				}
			})
			
		} else {
			
			return "Please login to an admin account to update products."
		}
	})
}

module.exports.archiveProduct = (data) => {
	
	return Product.findById(data.productId).then((result, err) => {
		
		if (data.isAdmin === true) {
			result.isActive = false;
			
			return result.save().then((archivedProduct, err) => {
				
				if (err) {
					
					return false;
					
				} else {
					
					return result;
				}
			}	)
		} else {
			return false;
		}
	})
}
