const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const req = require("express/lib/request");
const auth = require("../authentication/auth");

//Check if Email exists
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then( result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}


//User Registration
module.exports.registerUser = (reqBody) => {

		let newUser = new User({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
                isAdmin: reqBody.isAdmin,
				password: bcrypt.hashSync(reqBody.password, 10)
		})

		return newUser.save().then((user, error) => {

			if(error){
				return false
			} else{
				return true
			}
		})
}


//User Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result =>{

		if(result == null){
			return false
		} else {

			//compareSync(dataToBeCompared, encryptedData)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}

// Setting another user as Admin
module.exports.makeAdmin = (data) => {
    return User.findById(data.userId).then((result, error) => {
        console.log(result);

        if(data.isAdmin == true) {
            result.isAdmin = true;
            console.log(result);

            return result.save().then((updatedUser, error) => {

				if(error){
					return false;
				} else {
					return updatedUser;
				}
			})
        } else {
            return "Please login to an Admin account before setting others as Admin.";
        }
    })
}

// Creating order
module.exports.createOrder = async (data) => {
	
	console.log(data);
	
	if(data.isAdmin === true){
		
		return "Please login as regular user to checkout.";
		
	} else {
		
		let isUserUpdated = await User.findById(data.userId).then(user => {
			
			user.orders.push({productId: data.productId});
			
			return user.save().then((user, error) => {
				
				if(error){
					
					return false;
					
				} else {
					
					return true;
				}
			})
		})
		
		let isProductUpdated = await Product.findById(data.productId).then(product => {
			
			product.orders.push({userId: data.userId});
			
			return product.save().then((product, error) => {
				
				if(error){
					
					return false;
					
				} else {
					
					return true;
				}
			})
		})

		
		if(isUserUpdated && isProductUpdated){
			
			return true;
			
		} else {
			
			return false;
		}
	}
}

module.exports.retrieveAllOrders = (data) => {

	return User.find().then(result => {

		let totalOrders = [];
	
		result.forEach(user => {
			user.orders.forEach(purchase => {
				purchase.userId = user._id;
				totalOrders.push(purchase);
			})
		})

		if (data.isAdmin == true) {
			return totalOrders;
		} else {
			return "Please login as admin user to retrieve all orders.";
		}
	})
}

module.exports.retrieveOrder = (data) => {
	console.log(data);

	return User.findById(data.userId).then((result, err) => {
	console.log(result);

		if(data.isAdmin == false) {

			return result.orders;

		} else {
			return "Please login as regular user to retrieve individual user order.";
		}
	})
}

